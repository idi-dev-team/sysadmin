$RegKeyPath = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\PersonalizationCSP"
$DesktopPath = "DesktopImagePath"
$DesktopStatus = "DesktopImageStatus"
$DesktopUrl = "DesktopImageUrl"
$LockScreenPath = "LockScreenImagePath"
$LockScreenStatus = "LockScreenImageStatus"
$LockScreenUrl = "LockScreenImageUrl"
$StatusValue = "1"
$url1 = "http://files.idi-demo.com/images/blue-bg-logo-2560.png"
$url2 = "http://files.idi-demo.com/images/grey-bg-logo-2560.png"
$DesktopImageValue = "C:\MDM\wallpaper_desktop.jpg"
$LockScreenImageValue = "C:\MDM\wallpaper_lockscreen.jpg"
$directory = "C:\MDM\"


If ((Test-Path -Path $directory) -eq $false) {
	New-Item -Path $directory -ItemType directory
}

# Download images for desktop and lockscreen
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url1, $DesktopImageValue)
$wc2 = New-Object System.Net.WebClient
$wc2.DownloadFile($url2, $LockScreenImageValue)

if (!(Test-Path $RegKeyPath)) {
	Write-Host "Creating registry path $($RegKeyPath)."
	New-Item -Path $RegKeyPath -Force | Out-Null
}

# Set registry values
New-ItemProperty -Path $RegKeyPath -Name $DesktopStatus -Value $StatusValue -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $RegKeyPath -Name $DesktopPath -Value $DesktopImageValue -PropertyType STRING -Force | Out-Null
New-ItemProperty -Path $RegKeyPath -Name $DesktopUrl -Value $DesktopImageValue -PropertyType STRING -Force | Out-Null
New-ItemProperty -Path $RegKeyPath -Name $LockScreenStatus -Value $StatusValue -PropertyType DWORD -Force | Out-Null
New-ItemProperty -Path $RegKeyPath -Name $LockScreenPath -Value $LockScreenImageValue -PropertyType STRING -Force | Out-Null
New-ItemProperty -Path $RegKeyPath -Name $LockScreenUrl -Value $LockScreenImageValue -PropertyType STRING -Force | Out-Null

RUNDLL32.EXE USER32.DLL, UpdatePerUserSystemParameters 1, True