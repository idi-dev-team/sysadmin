$file = 'Authy.exe'
$link = "http://files.idi-demo.com/software/$file"
$soft_name = 'Authy'

$find = Get-WmiObject -Class Win32_Product -Filter "Name LIKE `'$soft_name`'"

if ($find -eq $null) {

    $tmp = "$env:TEMP\$file"
    $client = New-Object System.Net.WebClient
    $client.DownloadFile($link, $tmp)

    Start-Process -Wait -FilePath $tmp -ArgumentList "/S" -PassThru
    echo "Starting $soft_name install"
    Start-Sleep -s 60
    Write-Host "$soft_name installed"

} else {
    echo "ERROR: $soft_name is already installed."
    echo $find
    exit 1
}

exit 0