$file = 'Nsoftphone.msi'
$link = "http://files.idi-demo.com/software/$file"
$soft_name = 'Nsoftphone'

$find = Get-WmiObject -Class Win32_Product -Filter "Name LIKE `'$soft_name`'"

if ($find -eq $null) {

    $tmp = "$env:TEMP\$file"
    $client = New-Object System.Net.WebClient
    $client.DownloadFile($link, $tmp)

    msiexec.exe /i $tmp /passive /qn
    echo "Starting $soft_name install"
    Start-Sleep -s 60
    Write-Host "$soft_name installed"

} else {
    echo "ERROR: $soft_name is already installed."
    echo $find
    exit 1
}

exit 0