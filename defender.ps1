Set-MpPreference -ScanOnlyIfIdleEnabled $false
Set-MpPreference -ScanScheduleQuickScanTime 10:00:00
Set-MpPreference -RandomizeScheduleTaskTimes $false
Set-MpPreference -DisableCatchupQuickScan $false
Set-MpPreference -DisableCatchupFullScan $false
Set-MpPreference -SignatureScheduleDay Everyday
Set-MpPreference -SignatureScheduleTime 09:45:00