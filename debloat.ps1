﻿$apps = @(
    'Microsoft.SkypeApp',
    'Microsoft.XboxApp',
    'Microsoft.XboxGameOverlay',
    'Microsoft.XboxGamingOverlay',
    'Microsoft.XboxIdentityProvider',
    'Microsoft.XboxSpeechToTextOverlay',
    'Microsoft.YourPhone',
    'Microsoft.Microsoft3DViewer',
    '7EE7776C.LinkedInforWindows',
    'Microsoft.ZuneMusic',
    'Microsoft.ZuneVideo',
    'Microsoft.WindowsMaps',
    'Microsoft.Messaging',
    'Microsoft.MixedReality.Portal',
    'AD2F1837.HPJumpStart',
    '22094SynapticsIncorporate.AudioControls',
    'AD2F1837.HPPCHardwareDiagnosticsWindows',
    'Microsoft.BingWeather',
    'Microsoft.MicrosoftOfficeHub',
    'Microsoft.Office.OneNote',
    'Microsoft.OneConnect',
    'Microsoft.People',
    'Microsoft.Print3D',
    'Microsoft.ScreenSketch',
    'Microsoft.Windows.Photos',
    'Microsoft.WindowsAlarms',
    'Microsoft.WindowsFeedbackHub'
);

foreach($a in $apps) {
    $name =  Get-AppxPackage -allusers $a;
    if ($name -ne $null) {
        Write-Host "Removing $($name)";
        Remove-AppxPackage -Package $name -AllUsers
    }
}


# Remove HP Device Manager first as it is a dependency for other HP packages
Get-Package 'HP Device*' -ProviderName MSI | uninstall-package

# Remove most of the rest of the HP bloat
Get-Package 'HP*' -ProviderName MSI | uninstall-package

# Remove HP Documentation
start 'C:\Program Files\HP\Documentation\Doc_uninstall.cmd'